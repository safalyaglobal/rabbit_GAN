from keras.layers import Activation, Dense, Conv2D, Conv2DTranspose, Input, BatchNormalization, Flatten
from keras.optimizers import adam
from keras.models import Sequential
from keras import backend as K 
import numpy as np 

class models():

    def generator(self):

        if self.gen_net:
            return self.gen_net

        self.gen_net = Sequential()

        self.gen_net.add(Conv2D(filters = 32, kernel_size = 5, padding = 'valid', input_shape = [self.img_shape, self.img_shape, 3]))
        self.gen_net.add(BatchNormalization())
        self.gen_net.add(Activation(activation = 'relu'))

        self.gen_net.add(Conv2D(filters = 64, kernel_size = 5, padding = 'valid'))
        self.gen_net.add(BatchNormalization())
        self.gen_net.add(Activation(activation = 'relu'))
        
        self.gen_net.add(Conv2D(filters = 128, kernel_size = 5, padding = 'valid'))
        self.gen_net.add(BatchNormalization())
        self.gen_net.add(Activation(activation = 'relu'))
        
        self.gen_net.add(Conv2D(filters = 256, kernel_size = 5, padding = 'valid'))
        self.gen_net.add(BatchNormalization())
        self.gen_net.add(Activation(activation = 'relu'))
        
        self.gen_net.add(Conv2DTranspose(filters = 128, kernel_size = 5, padding = 'valid'))
        self.gen_net.add(BatchNormalization())
        self.gen_net.add(Activation(activation = 'relu'))

        self.gen_net.add(Conv2DTranspose(filters = 64, kernel_size = 5, padding = 'valid'))
        self.gen_net.add(BatchNormalization())
        self.gen_net.add(Activation(activation = 'relu'))

        self.gen_net.add(Conv2DTranspose(filters = 32, kernel_size = 5, padding = 'valid'))
        self.gen_net.add(BatchNormalization())
        self.gen_net.add(Activation(activation = 'relu'))

        self.gen_net.add(Conv2DTranspose(filters = 3, kernel_size = 5, padding = 'valid'))
        self.gen_net.add(BatchNormalization())
        self.gen_net.add(Activation(activation = 'sigmoid'))

        return self.gen_net
    
    def discriminator(self):

        if self.disc_net:
            return self.disc_net

        self.disc_net = Sequential()

        self.disc_net.add(Conv2D(filters = 8, kernel_size = 5, padding = 'valid', input_shape = [self.img_shape, self.img_shape, 3]))
        self.disc_net.add(BatchNormalization())
        self.disc_net.add(Activation(activation = 'relu'))

        self.disc_net.add(Conv2D(filters = 16, kernel_size = 5, padding = 'valid'))
        self.disc_net.add(BatchNormalization())
        self.disc_net.add(Activation(activation = 'relu'))

        self.disc_net.add(Conv2D(filters = 32, kernel_size = 7, padding = 'valid'))
        self.disc_net.add(BatchNormalization())
        self.disc_net.add(Activation(activation = 'relu'))

        self.disc_net.add(Conv2D(filters = 64, kernel_size = 7, padding = 'valid'))
        self.disc_net.add(BatchNormalization())
        self.disc_net.add(Activation(activation = 'relu'))

        self.disc_net.add(Flatten())
        self.disc_net.add(Dense(1, activation = 'sigmoid'))

        return self.disc_net  

    def discriminator_model(self):

        if self.disc_mdl:
            return self.disc_mdl
        
        opt = adam(lr = 0.001)
        self.disc_mdl = Sequential()
        self.disc_mdl.add(self.discriminator())
        self.disc_mdl.compile(optimizer = opt, loss = 'binary_crossentropy', metrics = ['accuracy'])

        return self.disc_mdl

    def adversarial_model(self):

        if self.adv_mdl:
            return self.adv_mdl
        
        opt = adam(lr = 0.0002)
        self.adv_mdl = Sequential()
        self.adv_mdl.add(self.generator())
        self.adv_mdl.add(self.discriminator())
        self.adv_mdl.compile(optimizer = opt, loss = 'binary_crossentropy', metrics = ['accuracy'])

        

    def __init__(self, img_shape):

        self.disc_net = None
        self.gen_net = None
        self.disc_mdl = None
        self.adv_mdl = None

        self.img_shape = img_shape