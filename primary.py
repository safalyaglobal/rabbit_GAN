from models import models as mdl 
from prep import prep as prp 
from keras.models import Model, load_model
import numpy as np

class prime():

    def __init__(self, force_reset = False):

        print('loading dataset...')
        self.dat = prp('Dataset', 'dat_true', 'dat_false/city')
        #Removes the raw datasets from memory
        print('flushing useless data...')
        self.dat.flush(True, False) 
        #Gets the dimensions of the images
        self.dims = self.dat.arr_true.shape

        print('checking for past models...')

        try:
            self.disc_mdl = load_model('disc_mdl.h5')
            self.adv_mdl = load_model('adv_mdl.h5')
            self.gen_net = load_model('gen_net.h5')

            if force_reset:
                raise ValueError('forced reset exception...')

            ('past models loaded...')

        except:
            print('past models not found, building new ones...')
            #creates a model with the average size of the images if models aren't loaded
            self.md = mdl(self.dims[1])

            self.disc_mdl = self.md.discriminator_model()
            self.adv_mdl = self.md.adversarial_model()
            self.gen_net = self.md.generator()

    def train(self, epochs = 2000, batch_size = 5):
        for epoch in range(epochs):

            imgs_train_true = self.dat.arr_true[np.random.randint(self.dims[0], size = batch_size)]
            imgs_train_false = self.dat.arr_false[np.random.randint(self.dims[0], size = batch_size)]

            imgs_train_fake = self.gen_net.predict(imgs_train_false)
            X = np.concatenate((imgs_train_true, imgs_train_fake))

            Y = np.ones(shape = [2 * batch_size, 1]) * 0.75
            Y[batch_size :, :] = 0

            disc_loss = self.disc_mdl.train_on_batch(X, Y)

            Y = np.ones(shape = [batch_size, 1])
            adv_loss = self.adv_mdl.train_on_batch(imgs_train_false, Y)

            log_metric = '%d : [Disc loss : %f ; acc : %f]' % (epoch , disc_loss[0], disc_loss[1])
            log_metric = '%s ;; [Adv loss : %f ; acc : %f]' % (log_metric, adv_loss[0], adv_loss[1])
            print(log_metric)


        print('saving models for later use...')
        self.disc_mdl.save('disc_mdl.h5')
        self.adv_mdl.save('adv_mdl.h5')
        self.gen_net.save('gen_net,h5') 

if __name__ == '__main__':

    prime_mdl = prime(True)
    prime_mdl.train()

