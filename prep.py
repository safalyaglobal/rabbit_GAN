from numpy import random as rand 
import tensorflow as tf 
import numpy as np
import os
from PIL import Image
 
class prep():

    #Import
    def fetch_imgs(self):

        true_list =  os.listdir(self.true_path)
        false_list = os.listdir(self.false_path)
        
        self.raw_true = []
        self.raw_false = []
        
        for img in true_list:
            self.raw_true.append(np.array(Image.open(self.true_path + '/' + img)))
        
        for img in false_list:
            self.raw_false.append(np.array(Image.open(self.false_path + '/' + img)))

        #Import raw images

        self.arr_true = self.resize(self.raw_true)
        self.arr_false = self.resize(self.raw_false)

    #Resize   
    def resize(self, imgs, shape = [300, 300]):

        r_imgs_list = []

        for img in imgs:       

            X = tf.placeholder(tf.float32, shape = [None, None, 3])

            X_resized = tf.image.resize_images(X, size = shape)

            with tf.Session() as sess:
                r_imgs_list.append(sess.run(X_resized, feed_dict = {X : img}))
        
        return np.array(r_imgs_list)

    #Save

    #Flush
    def flush(self, raw = False, processed = False):
        if raw :
            self.raw_true = None
            self.raw_false = None
        if processed :
            self.arr_true = None
            self.arr_false = None

    #Init
    def __init__(self, dat_folder, true_folder, false_folder):
        self.dat_path = os.path.abspath(dat_folder)
        self.true_path = os.path.abspath(dat_folder + '/' + true_folder)
        self.false_path = os.path.abspath(dat_folder + '/' + true_folder)

        self.fetch_imgs()